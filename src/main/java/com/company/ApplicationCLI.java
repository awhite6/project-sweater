package com.company;

import com.company.model.Forecast;
import com.company.model.Recommendation;
import com.company.utility.HttpClient;
import com.company.utility.JsonReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ApplicationCLI {
    private final String FILE_NAME = "Recommendations.json";
    private final String CITY_PROMPT = "Please enter a City: ";
    private final String STATE_PROMPT = "Please enter a State: ";
    private final String RESTART_PROMPT = "Type restart to enter another city: ";
    private Scanner in;
    private List<Recommendation> allRecommendations;
    private JsonReader jsonReader;
    private HttpClient httpClient;

    public ApplicationCLI() {
        this.httpClient = new HttpClient();
        this.jsonReader = new JsonReader();
        this.in = new Scanner(System.in);
    }

    public List<Recommendation> getAllRecommendations() { return this.allRecommendations; }
    public void setAllRecommendations(List<Recommendation> allRecommendations) { this.allRecommendations = allRecommendations; }

    public void run() throws IOException {
        setAllRecommendations(jsonReader.getRecommendationsAsList(FILE_NAME));
        String city = "";
        String state = "";
        while (true) {

            while (city.length() == 0) {
                city = userInputPrompt(CITY_PROMPT);
            }

            printEmptyLine();

            while (state.length() == 0) {
                state = userInputPrompt(STATE_PROMPT);
            }

            printEmptyLine();

            List<Forecast> forecasts = httpClient.getWeatherByCityAndState(city.trim(), state.trim());

            if (forecasts.size() == 0) {
                System.out.println("Invalid City or State");
            } else {
                List<Recommendation> recommendedClothing = filterRecommendationsByForecast(forecasts.get(0));
                printRecommendedClothingForForecast(recommendedClothing, forecasts.get(0));
            }

            if (userInputPrompt(RESTART_PROMPT).equals("restart")) {
                city = "";
                state = "";
                printEmptyLine();
                continue;
            }
            break;
        }
    }

    public List<Recommendation> filterRecommendationsByForecast(Forecast forecast) {
        List<Recommendation> recommendationsForWeather = new ArrayList<>();
        for (Recommendation r : getAllRecommendations()) {
            int forecastTemp = forecast.getTemperature();

            if (!checkIfWeatherRequiresWaterproof(forecast) &&
                    forecastTemp >= r.getMinTemp() && forecastTemp <= r.getMaxTemp()) {

                recommendationsForWeather.add(r);

            } else if (forecastTemp >= r.getMinTemp() && forecastTemp <= r.getMaxTemp() && r.getIsWaterproof()) {
                recommendationsForWeather.add(r);
            }
        }

        return recommendationsForWeather;
    }

    private void printRecommendedClothingForForecast(List<Recommendation> recommendedClothing, Forecast forecast) {
        System.out.println("TEMPERATURE: " + forecast.getTemperature());
        System.out.println("WEATHER: " + forecast.getWeather());
        System.out.println("-- RECOMMENDED CLOTHING --");
        for (Recommendation r : recommendedClothing) {
            System.out.println("- " + r.getName());
        }
    }

    private boolean checkIfWeatherRequiresWaterproof(Forecast forecast) {
        return (forecast.getWeather() == Weather.RAIN || forecast.getWeather() == Weather.DRIZZLE ||
                forecast.getWeather() == Weather.SNOW || forecast.getWeather() == Weather.THUNDERSTORM);
    }

    private String userInputPrompt(String display) {
        System.out.println(display);
        return in.nextLine();
    }

    private void printEmptyLine() {
        System.out.println("");
    }
}
