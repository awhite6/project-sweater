package com.company;

public enum Weather {
    CLOUDS,
    CLEAR,
    SNOW,
    RAIN,
    DRIZZLE,
    THUNDERSTORM,
    MIST,
    SMOKE,
    HAZE,
    DUST,
    FOG,
    SAND,
    ASH,
    SQUALL,
    TORNADO
}
