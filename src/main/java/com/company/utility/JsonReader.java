package com.company.utility;

import com.company.Weather;
import com.company.model.Forecast;
import com.company.model.Recommendation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonReader {
    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public JsonReader() {}

    public List<Recommendation> getRecommendationsAsList(String fileName) {
        List<Recommendation>  recommendations = new ArrayList<>();

        try (InputStream inputStream = JsonReader.class.getClassLoader().getResourceAsStream(fileName)) {
            String availableRecommendations = OBJECT_MAPPER.readValue(inputStream, JsonNode.class)
                                              .get("available_recommendations").toString();
            recommendations = Arrays.asList(OBJECT_MAPPER.readValue(availableRecommendations, Recommendation[].class));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recommendations;
    }

    public List<Forecast> getForecastList(String response) {
        List<Forecast> forecastList = new ArrayList<>();

        try {
            String json = OBJECT_MAPPER.readValue(response, JsonNode.class).get("list").toString();
            JsonNode[] list = OBJECT_MAPPER.readValue(json, JsonNode[].class);
            for (JsonNode node : list) {
                Forecast forecast = new Forecast();
                forecast.setTemperature(node.get("main").get("temp").intValue());
                forecast.setWeather(Weather.valueOf(node.get("weather").get(0).get("main").textValue().toUpperCase()));
                forecastList.add(forecast);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return forecastList;
    }
}
