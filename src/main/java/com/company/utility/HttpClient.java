package com.company.utility;

import com.company.model.Forecast;
import okhttp3.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpClient {
    private final String API_KEY = "&appid=ef406a2f64fc8d7fd75871e43c2f9cd2";
    private final String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast?q=";
    private final String UNITS = "&units=imperial";
    private final String COUNT = "&cnt=3";
    private final String COUNTRY_CODE = "USA";
    private OkHttpClient client;
    private JsonReader jsonReader;

    public HttpClient() {
        client = new OkHttpClient();
        jsonReader = new JsonReader();
    }

    public List<Forecast> getWeatherByCityAndState(String city, String stateCode) throws IOException {
        List<Forecast> forecastList = new ArrayList<>();
        Request request = new Request.Builder()
                .url(BASE_URL + city + "," + stateCode + "," + COUNTRY_CODE + UNITS + COUNT + API_KEY)
                .build();

        Call call = client.newCall(request);
        Response response = call.execute();

        if (response.isSuccessful()) {
            forecastList = jsonReader.getForecastList(response.body().string());
        }

        response.body().close();

        return forecastList;
    }
}
