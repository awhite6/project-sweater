package com.company.model;

import com.company.Weather;

public class Forecast {
    private int temperature;
    private Weather weather;

    public Forecast() {}

    public Forecast(int temperature, Weather weather) {
        this.temperature = temperature;
        this.weather = weather;
    }

    public int getTemperature() { return this.temperature; }
    public Weather getWeather() { return this.weather; }

    public void setTemperature(int temperature) { this.temperature = temperature; }
    public void setWeather(Weather weather) { this.weather = weather; }
}
