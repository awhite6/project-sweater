package com.company.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recommendation {
    private String name;
    @JsonProperty("waterproof")
    private boolean isWaterproof;
    @JsonProperty("min_temp")
    private int minTemp;
    @JsonProperty("max_temp")
    private int maxTemp;

    public Recommendation() {}

    public Recommendation(String name, boolean isWaterproof, int minTemp, int maxTemp) {
        this.name = name;
        this.isWaterproof = isWaterproof;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public String getName() {
        return this.name;
    }
    public boolean getIsWaterproof() {
        return this.isWaterproof;
    }
    public int getMinTemp() {
        return this.minTemp;
    }
    public int getMaxTemp() {
        return this.maxTemp;
    }

    public void setName(String name) { this.name = name; }
    public void setIsWaterproof(boolean isWaterproof) { this.isWaterproof = isWaterproof; }
    public void setMinTemp(int minTemp) { this.minTemp = minTemp; }
    public void setMaxTemp(int maxTemp) { this.maxTemp = maxTemp; }
}
