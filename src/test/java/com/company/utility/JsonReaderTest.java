package com.company.utility;

import com.company.Weather;
import com.company.model.Forecast;
import com.company.model.Recommendation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonReaderTest {
    private final String RECOMMENDATIONS_FILE = "TestRecommendations.json";
    private final String MOCK_FORECAST_FILE = "MockForecastData.json";
    private JsonReader jsonReader;
    private ObjectMapper objectMapper;
    private List<Recommendation> expectedRecommendations;
    private List<Forecast> expectedForecasts;
    private String mockForecastData;

    @Before
    public void setup() {
        jsonReader = new JsonReader();
        objectMapper = new ObjectMapper();
        expectedRecommendations = new ArrayList<>(Arrays.asList(
                new Recommendation("Parakeet Jones", false, 10, 15),
                new Recommendation("Joey Pizza", false, 70, 80),
                new Recommendation("Donkey Kong", true, 45, 77)
        ));
        expectedForecasts = new ArrayList<>(Arrays.asList(
                new Forecast(70, Weather.CLEAR),
                new Forecast(28, Weather.SNOW),
                new Forecast(55, Weather.CLOUDS)
        ));
    }

    @Test
    public void returns_expected_parsed_recommendations() {
        List<Recommendation> recommendationList = jsonReader.getRecommendationsAsList(RECOMMENDATIONS_FILE);
        for (int i = 0; i < recommendationList.size(); i++) {
            Recommendation result = recommendationList.get(i);
            Recommendation expected = expectedRecommendations.get(i);

            Assert.assertEquals(expected.getName(), result.getName());
            Assert.assertEquals(expected.getIsWaterproof(), result.getIsWaterproof());
            Assert.assertEquals(expected.getMinTemp(), result.getMinTemp());
            Assert.assertEquals(expected.getMaxTemp(), result.getMaxTemp());
        }
    }

    @Test
    public void returns_expected_parsed_forecasts() {
        try  (InputStream inputStream = JsonReader.class.getClassLoader().getResourceAsStream(MOCK_FORECAST_FILE)){
            mockForecastData = objectMapper.readValue(inputStream, JsonNode.class).toString();
            List<Forecast> forecastList = jsonReader.getForecastList(mockForecastData);
            for (int i = 0; i < forecastList.size(); i++) {
                Forecast expected = expectedForecasts.get(i);
                Forecast result = forecastList.get(i);

                Assert.assertEquals(expected.getWeather(), result.getWeather());
                Assert.assertEquals(expected.getTemperature(), result.getTemperature());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void returns_parsed_recommendations_as_list() {
        Assert.assertTrue(jsonReader.getRecommendationsAsList(RECOMMENDATIONS_FILE) instanceof List<?>);
    }

    @Test
    public void returns_parsed_forecast_as_list() {
        try  (InputStream inputStream = JsonReader.class.getClassLoader().getResourceAsStream(MOCK_FORECAST_FILE)){
            mockForecastData = objectMapper.readValue(inputStream, JsonNode.class).toString();
            Assert.assertTrue(jsonReader.getForecastList(mockForecastData) instanceof  List<?>);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void returns_empty_list_when_given_invalid_json() {
        Assert.assertEquals(jsonReader.getForecastList("").size(), 0);
        Assert.assertEquals(jsonReader.getRecommendationsAsList("").size(), 0);
    }
}
