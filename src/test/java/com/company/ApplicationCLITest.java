package com.company;

import com.company.model.Forecast;
import com.company.model.Recommendation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ApplicationCLITest {
    private ApplicationCLI app;
    private List<Recommendation> allRecommendations;

    @Before
    public void setup() {
        app = new ApplicationCLI();
        allRecommendations = Arrays.asList(
                new Recommendation("Sweater", false, 55, 80),
                new Recommendation("Rain Coat", true, 40, 80),
                new Recommendation("Snow Boots", true, 0, 32),
                new Recommendation("Heavy Coat", true, 0, 39)
        );
    }

    @Test
    public void returns_expected_recommendations_when_filtering_for_a_forecast() {
        Forecast forecast = new Forecast(31, Weather.SNOW);
        List<Recommendation> expectedRecommendations = Arrays.asList(
                new Recommendation("Snow Boots", true, 0, 32),
                new Recommendation("Heavy Coat", true, 0, 39)
        );
        app.setAllRecommendations(allRecommendations);
        List<Recommendation> filteredRecommendations = app.filterRecommendationsByForecast(forecast);

        for (int i = 0; i < filteredRecommendations.size(); i++) {
            compareRecommendations(expectedRecommendations.get(i), filteredRecommendations.get(i));
        }

        forecast = new Forecast(66, Weather.RAIN);
        filteredRecommendations = app.filterRecommendationsByForecast(forecast);
        expectedRecommendations = Arrays.asList(
                new Recommendation("Rain Coat", true, 40, 80)
        );

        for (int i = 0; i < filteredRecommendations.size(); i++) {
            compareRecommendations(expectedRecommendations.get(i), filteredRecommendations.get(i));
        }
    }

    private void compareRecommendations(Recommendation expected, Recommendation result) {
        Assert.assertEquals(expected.getName(), result.getName());
        Assert.assertEquals(expected.getIsWaterproof(), result.getIsWaterproof());
        Assert.assertEquals(expected.getMinTemp(), result.getMinTemp());
        Assert.assertEquals(expected.getMaxTemp(), result.getMaxTemp());
    }
}
