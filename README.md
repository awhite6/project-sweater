# PROJECT S.W.E.A.T.E.R
## Run Instructions
Personally I would just run it in my IDE (Intellij in my case). However you can create a jar by running mvn clean package and executing the jar found in the target folder. See below for commands.

```bash
mvn clean package
cd target
java -jar <jar_name>
```

## Thoughts


I decided to use Java because I have made CLI apps in Java before and it is what I've used the most in terms of backend throughout my career. However about halfway through I wished I had chosen a more functional language to make working with the data a little less clunky/require less setup.

I chose to format the Recommendations in JSON rather than yaml simply because I find it easier to read. I had made a utility class in the project to parse the json into objects. At first I wanted to make it super generic so as it could have been used with just one method instead of multiple to fit my needs but I figured that would have taken way too long for the scope of this challenge. 

I didn't start writing tests until a little bit in because I felt I was on a roll and didn't want to stop. Unfortunately I didn't really get to write nearly as many tests as I usually would because the beginning stages of the project took a little longer than anticipated. Could have remedied this by writing tests while I was writing the code previously but I didn't.

I decided to use Junit and Mockito for my tests as that is what I've always used.I added mock JSON files in the resources folder in the test directory so I could just parse them into strings instead of trying to make messy mock data in the actual test files. I never got around to making tests to mock connections to the api unfortunately so Mockito was never actually used. I left it in the POM (as well as some other empty test files) to show some of my process. Those would probably the things I would work on if I had more time.

For connecting to the actual weather api I decided to use OkHttpClient. I've messed around with it before and I found it to be fairly simple and easy to read. It didn't require much set up.

I made an enum of all the weather 'types' from the api just to avoid having to use strings everywhere. I just found it easier to read and it kinda keeps everything more consistent. The downside being if they update their api and add more types.

Originally I wanted to use multiple 'Forecasts' over one day and give recommendations as the day changed (Weather isn't always consistent) but for the sake of time I only got the first recommendation of the day. 

In the end I probably had way more fun making this little app than I should have and wish I had more time to flesh it out. I'm disappointed with how little tests I was able to get in but I wanted to have a finished product and didn't want to go too long over the recommended 4 hour limit.
